# utos-msgpack

# CHANGELOG

## [0.2.9]

* Add button to copy text from file to input area

## [0.2.8]

* Request timeout field

## [0.2.7]

* Updating raid

## [0.2.6]

* Change delimiter field from textfield to combobox

## [0.2.5]

* Fix function call to `disable-elements-connecting`

## [0.2.4]

* Add shortcut to close

## [0.2.3]

* Updating raid client

## [0.2.2]

* UDP
* I18N
* About gui
* Main gui
