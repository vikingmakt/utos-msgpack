(defproject utos-msgpack "0.2.9"
  :description "UTOS MsgPack UI Client"
  :url "https://gitlab.com/vikingmakt/utos-msgpack"
  :license {:name "BSD-2-Clause"}

  :dependencies [[br.com.vikingmakt/augustus "0.1.12"]
                 [br.com.vikingmakt/raid "1.5.7"]
                 [cheshire "5.8.1"]
                 [org.apache.logging.log4j/log4j-core "2.11.2"]
                 [org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.logging "0.4.1"]]

  :main utos-msgpack.launcher
  :aot [utos-msgpack.launcher]
  :java-source-paths ["java/src/"]

  :auto-clean true
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :release {:aot :all
                       :uberjar-name "utos-msgpack-release.jar"}}
  :jvm-opts ["-Xms30m" "-Xmx60m"]

  :aliases {"launch" ["do" "clean," "run"]
            "make-jar" ["with-profile" "release" "uberjar"]
            "release" ["do" "make-jar," "launch4j"]})
