(ns utos-msgpack.i18n
  (:import [java.util Locale ResourceBundle])
  (:gen-class))

(defonce bundle (atom nil))

(defn language->locale [lang country]
  (if country
    (new Locale lang country)
    (new Locale lang)))

(defn get-bundle* []
  (if-not @bundle
    (let [locale (Locale/getDefault)]
      (reset! bundle {:res (ResourceBundle/getBundle "i18n.translate" locale)
                      :lang (.getLanguage locale)
                      :country (.getCountry locale)})))
  @bundle)

(defn get-bundle []
  (:res (get-bundle*)))

(defn set-bundle [lang & [country]]
  (let [locale (language->locale lang country)]
    (reset! bundle {:res (ResourceBundle/getBundle "i18n.translate" ^Locale locale)
                    :lang lang
                    :country country})))

(defn get-string [key]
  (.getString ^ResourceBundle (:res (get-bundle*)) key))

(defn add-listener [key cb]
  (->> (fn [key ref old-val new-val]
         (if old-val
           (if-not (and (= (:lang old-val) (:lang new-val)) (= (:country old-val) (:country new-val)))
             (cb key (:lang new-val) (:country new-val)))))
       (add-watch bundle key)))
