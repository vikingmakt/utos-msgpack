(ns utos-msgpack.view-models.main
  (:require [augustus.future :as afuture]
            [cheshire.core :as json]
            [raid.core :as raid]
            [raid.socket :as raid-socket]
            [utos-msgpack.concurrent :refer [run-later run-task]])
  (:import [java.net InetAddress InetSocketAddress]
           [javafx.beans.property SimpleBooleanProperty SimpleObjectProperty SimpleStringProperty]
           javafx.scene.paint.Color)
  (:gen-class))

(defprotocol VMMain
  (add-handler [this callback])
  (close [this])
  (connect [this type host port opt])
  (get-status [this])
  (send-msg [this msg delimiter timeout]))

(defn- jsonify [value]
  (json/generate-string value {:escape-non-ascii false
                               :pretty (-> json/default-pretty-print-options
                                           (assoc :indent-arrays? true)
                                           json/create-pretty-printer)}))

(defn- send-server-msg [conn data msg ^String delimiter timeout prom]
  (try
    (let [{:keys [header body]} (json/parse-string msg true)
          {:keys [action]} header
          timeout (if timeout (try (Long/parseLong timeout) (catch Exception e)))
          timeout (or timeout 30000)
          delimiter (if delimiter (.getBytes delimiter "UTF-8"))]
      (afuture/add-future-callback
       (raid/request conn action body header {:delimiter delimiter
                                              :timeout timeout})
       (fn [res]
         (prom true)
         (try
           (reset! (:output data) (jsonify @res))
           (catch Exception e
             (reset! (:output data) (str e)))))))
    (catch Exception e
      (prom true)
      (reset! (:output data) (str e)))))

(defn- get-hostname [^InetSocketAddress address]
  (.getHostName address))

(defn thread-interrupt [^Thread t]
  (.interrupt t))

(defn open-connection [conn-obj type hostname port opt]
  (try
    (let [port (Long/parseLong port)
          local-port (if (= type :udp) (Long/parseLong (:local-port opt)))
          connector (case type
                      :tcp raid/connect-tcp
                      :udp raid/connect-udp)]
      (->> (assoc opt :local-port local-port :encoding :msgpack)
           (connector hostname port)
           (reset! conn-obj))
      nil)
    (catch Throwable e
      {:error-header "Cannot connect" :error-msg (str e)})))

(defn status-changed [{:keys [conn-status]} key ref old-val new-val]
  (reset! conn-status new-val))

(defn create-view []
  (let [conn (atom nil)
        data {:conn-status (atom nil)
              :output (atom nil)}]
    (reify
      VMMain
      (add-handler [_ callback]
        (doseq [[prop-name prop] data]
          (add-watch prop prop-name callback)))
      (close [_]
        (if @conn
          (raid/close @conn)))
      (connect [_ type host port opt]
        (reset! (:conn-status data) :NEW)
        (run-task
         (if-let [err (open-connection conn type host port opt)]
           (do
             (reset! (:conn-status data) :CONN-FAILED)
             (assoc err :ok? false))
           (do
             (reset! (:conn-status data) :OPEN)
             (raid-socket/status-add-listener @conn :disconnect (fn [& args] (apply status-changed data args)))
             {:ok? true}))))
      (get-status [_]
        (if @conn
          (raid-socket/status @conn)
          :CLOSED))
      (send-msg [_ msg delimiter timeout]
        (run-task
         (let [p (promise)]
           (send-server-msg @conn data msg delimiter timeout p)
           @p))))))
