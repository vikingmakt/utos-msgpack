(ns utos-msgpack.gui
  (:require [clojure.java.io :as io]
            [utos-msgpack.concurrent :refer [fx-run run-later run-now]]
            [utos-msgpack.i18n :as i18n])
  (:import java.io.InputStream
           [java.util Collection List Optional]
           java.util.function.Consumer
           javafx.beans.value.ChangeListener
           javafx.collections.ObservableList
           [javafx.event Event EventHandler]
           javafx.fxml.FXMLLoader
           [javafx.geometry Rectangle2D Side]
           [javafx.scene Node Scene]
           [javafx.scene.control Alert Alert$AlertType Button ButtonBar$ButtonData ButtonType CheckBox ComboBoxBase ContextMenu Labeled MenuItem TextField TextInputControl ToggleButton]
           [javafx.scene.image Image ImageView]
           [javafx.scene.input KeyCode KeyCodeCombination KeyCombination KeyCombination$Modifier MouseEvent]
           javafx.scene.web.HTMLEditor
           [javafx.stage FileChooser FileChooser$ExtensionFilter Screen Stage])
  (:gen-class))

(def ^:private alert-types
  {:confirm Alert$AlertType/CONFIRMATION
   :error Alert$AlertType/ERROR
   :info Alert$AlertType/INFORMATION
   :none Alert$AlertType/NONE
   :warn Alert$AlertType/WARNING})

(def ^:private sides-types
  {:bot Side/BOTTOM})

(def ^:private key-mods
  {:alt KeyCombination/ALT_DOWN
   :ctrl KeyCombination/CONTROL_DOWN
   :meta KeyCombination/META_DOWN
   :shift KeyCombination/SHIFT_DOWN})

(defn consume [^Event ev]
  (.consume ev))

(defn add-shortcut [^Stage stage modifier key-code handler]
  (let [kc (KeyCode/getKeyCode key-code)
        mod (get key-mods modifier)]
    (cond
      (nil? kc) (throw (new Exception (format "%s is not a valid key code" key-code)))
      (nil? mod) (throw (new Exception (format "%s is not a valid modifier" modifier)))
      :else
      (-> stage
          .getScene
          .getAccelerators
          (.put (new KeyCodeCombination kc (into-array KeyCombination$Modifier [mod])) handler)))))

(defn add-icon [stage img-url]
  (.add (.getIcons ^Stage stage) (new Image (io/input-stream img-url))))

(defn on-click [^Node node handler]
  (.setOnMouseClicked node (proxy [EventHandler] []
                             (handle [e]
                               (handler)
                               (consume e)))))

(defn on-button-click [^Button button handler]
  (.setOnAction button (proxy [EventHandler] []
                         (handle [e]
                           (handler)
                           (consume e)))))

(defn on-menu-click [^MenuItem el handler]
  (.setOnAction el (proxy [EventHandler] []
                     (handle [e]
                       (handler)
                       (consume e)))))

(defn on-text-enter [^TextField tf handler]
  (.setOnAction tf (proxy [EventHandler] []
                     (handle [e]
                       (handler)
                       (consume e)))))

(defn on-text-changed [^TextField tf handler]
  (-> tf .textProperty (.addListener (proxy [ChangeListener] []
                                       (changed [ob old-val new-val]
                                         (handler old-val new-val))))))

(defn on-focus-changed [^Node el handler]
  (-> el .focusedProperty (.addListener (proxy [ChangeListener] []
                                          (changed [_ old-val new-val]
                                            (handler old-val new-val))))))

(defn on-context-menu-hiding [^ContextMenu cm handler]
  (.setOnHiding cm (proxy [EventHandler] []
                     (handle [e]
                       (handler)
                       (consume e)))))

(defn on-close-request [^Stage stage handler]
  (.setOnCloseRequest stage (proxy [EventHandler] []
                              (handle [e]
                                (handler)))))

(defn on-mouse-clicked [^Node el handler]
  (.addEventHandler el MouseEvent/MOUSE_CLICKED (proxy [EventHandler] []
                                                  (handle [e]
                                                    (handler)
                                                    (consume e)))))

(defn on-checkbox-changed [^CheckBox cb handler]
  (-> cb .selectedProperty (.addListener (proxy [ChangeListener] []
                                           (changed [& args]
                                             (apply handler args))))))

(defn on-key-pressed [^Scene scene handle]
  (.setOnKeyPressed scene (proxy [EventHandler] []
                            (handle [e]
                              (handle e)
                              (consume e)))))

(defn on-toggle-changed [^ToggleButton cb handler]
  (-> cb .selectedProperty (.addListener (proxy [ChangeListener] []
                                           (changed [& args]
                                             (apply handler args))))))

(defn get-rect-width [^Rectangle2D rect]
  (.getWidth rect))

(defn get-rect-min-x [^Rectangle2D rect]
  (.getMinX rect))

(defn get-rect-min-y [^Rectangle2D rect]
  (.getMinY rect))

(defn ol-add [^ObservableList ol value]
  (.add ol value))

(defn file-chooser [stage & {:keys [title ext op]}]
  (fx-run
   (let [fc (new FileChooser)]
     (if title (.setTitle fc title))
     (if ext
       (loop [ext ext]
         (if (seq ext)
           (let [[desc dot] (first ext)]
             (-> fc .getExtensionFilters (ol-add (new FileChooser$ExtensionFilter ^String desc ^List dot)))
             (recur (rest ext))))))
     (case op
       :save (.showSaveDialog fc stage)
       (.showOpenDialog fc stage)))))

(defn get-visual-bounds []
  (.getVisualBounds (Screen/getPrimary)))

(defn load-template [path]
  (FXMLLoader/load (io/resource (str "gui/fx/" path ".fxml")) (i18n/get-bundle)))

(defn loader [template]
  (new FXMLLoader (io/resource (str "gui/fx/" template ".fxml")) (i18n/get-bundle)))

(defn create-stage []
  (new Stage))

(defn create-scene [root]
  (new Scene root))

(defmulti get-text
  (fn [el]
    (cond
      (instance? Labeled el) :label
      (instance? TextInputControl el) :textinput)))

(defmethod get-text :textinput [^TextInputControl el]
  (.getText el))

(defmethod get-text :label [^Labeled el]
  (.getText el))

(defn set-placeholder [^TextInputControl el text]
  (.setPromptText el text))

(defmulti set-text
  (fn [el text]
    (cond
      (instance? Labeled el) :label
      (instance? MenuItem el) :menuitem
      (instance? TextInputControl el) :textinput)))

(defmethod set-text :label [^Labeled el ^String text]
  (fx-run (.setText el text)))

(defmethod set-text :menuitem [^MenuItem el ^String text]
  (fx-run (.setText el text)))

(defmethod set-text :textinput [^TextInputControl el ^String text]
  (fx-run (.setText el text)))

(defn set-stage-dimension [^Stage stage {:keys [width pos-x pos-y]}]
  (if width
    (.setWidth stage width))
  (if pos-x
    (.setX stage pos-x))
  (if pos-y
    (.setY stage pos-y)))

(defn set-stage-min-dimension [^Stage stage {:keys [height width]}]
  (if width
    (.setMinWidth stage width))
  (if height
    (.setMinHeight stage height)))

(defn get-value [el]
  (.getValue ^ComboBoxBase el))

(defn set-value [el v]
  (fx-run (.setValue ^ComboBoxBase el v)))

(defn set-disable [el v]
  (fx-run (.setDisable ^Node el v)))

(defn checkbox-selected? [el]
  (.isSelected ^CheckBox el))

(defn radio-selected? [el]
  (.isSelected ^ToggleButton el))

(defn radio-set [el v]
  (fx-run (.setSelected ^ToggleButton el v)))

(defn menu-hide [el]
  (fx-run (.hide ^ContextMenu el)))

(defn menu-show [el anchor side x y]
  (fx-run (.show ^ContextMenu el anchor (get sides-types side) x y)))

(defn set-visible [^Node el v]
  (fx-run (.setVisible el v)))

(defn htmleditor-get-text [el]
  (.getHtmlText ^HTMLEditor el))

(defn htmleditor-set-text [el v]
  (fx-run (.setHtmlText ^HTMLEditor el v)))

(defmulti set-image
  (fn [el v]
    (cond
      (instance? InputStream v) :stream
      (instance? Image v) :img)))

(defmethod set-image :stream [^ImageView el ^InputStream stream]
  (fx-run (.setImage el (new Image stream))))

(defmethod set-image :img [^ImageView el ^Image img]
  (fx-run (.setImage el img)))

(defn optional-if-present [^Optional op ^Consumer consumer]
  (.ifPresent op consumer))

(defn ol-set-all [^ObservableList ol ^Collection elements]
  (.setAll ol elements))

(defn show-alert-yes-no [type title msg yes-text no-text & [{:keys [header callback]}]]
  (optional-if-present
   (fx-run
    (let [alert (doto (new Alert (get alert-types type))
                  (.setTitle title)
                  (.setHeaderText header)
                  (.setContentText msg))
          t-yes (new ButtonType yes-text ButtonBar$ButtonData/YES)
          t-no (new ButtonType no-text ButtonBar$ButtonData/NO)]
      (-> alert .getDialogPane (.setStyle "-fx-background-color: rgb(50, 50, 50);"))
      (-> alert .getDialogPane .getStylesheets (.add (.toExternalForm (io/resource "gui/fx/asserts/css/modena_dark.css"))))
      (-> alert .getButtonTypes (ol-set-all [t-yes t-no]))
      (.showAndWait alert)))
   (proxy [Consumer] []
     (accept [^ButtonType e]
       (callback (.getButtonData e))))))

(defn show-alert [type title header msg & [show?]]
  (fx-run
   (let [alert (doto (new Alert (get alert-types type))
                 (.setTitle title)
                 (.setHeaderText header)
                 (.setContentText msg))]
     (-> alert .getDialogPane (.setStyle "-fx-background-color: rgb(50, 50, 50);"))
     (-> alert .getDialogPane .getStylesheets (.add (.toExternalForm (io/resource "gui/fx/asserts/css/modena_dark.css"))))
     (if show?
       (.showAndWait alert)))))

(defn set-title [stage title]
  (fx-run (.setTitle ^Stage stage title)))

(defn set-resizable [stage v]
  (fx-run (.setResizable ^Stage stage v)))

(defn show! [stage]
  (fx-run (.show ^Stage stage)))

(defn hide! [stage]
  (fx-run (.hide ^Stage stage)))

(defn close! [stage]
  (fx-run (.close ^Stage stage)))

(defn set-controller [loader controller]
  (fx-run (.setController ^FXMLLoader loader controller)))

(defn set-scene [stage scene]
  (fx-run (.setScene ^Stage stage scene)))

(defn setup-scene
  ([template controller]
   (let [loader (loader template)]
     (set-controller loader controller)
     (new Scene (.load ^FXMLLoader loader))))
  ([stage template controller]
   (let [scene (setup-scene template controller)]
     (set-scene stage scene)
     scene)))

(defn new-stage []
  (new Stage))
