(ns utos-msgpack.concurrent
  (:import javafx.application.Platform
           [javafx.concurrent Task WorkerStateEvent]
           javafx.event.EventHandler)
  (:gen-class))

(defn fx-thread? []
  (Platform/isFxApplicationThread))

(defn run-later* [f]
  (Platform/runLater f))

(defmacro run-later [& body]
  `(run-later* (fn [] (list ~@body))))

(defn run-now* [f]
  (let [p (promise)]
    (run-later
     (deliver p (try (f) (catch Throwable e e))))
    @p))

(defmacro run-now [& body]
  `(run-now* (fn [] ~@body)))

(defmacro fx-run [& body]
  `(if (fx-thread?)
     (do ~@body)
     (run-now ~@body)))

(defmacro run-task [& body]
  `(let [t# (proxy [Task] []
              (call [] ~@body))]
     (doto (new Thread t#)
       (.setDaemon true)
       .start)
     t#))

(defn set-on-succeeded [^Task task handler]
  (.setOnSucceeded task (proxy [EventHandler] []
                          (handle [^WorkerStateEvent e] (handler (-> e .getSource .getValue))))))
