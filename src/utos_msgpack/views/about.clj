(ns utos-msgpack.views.about
  (:require [clojure.java.io :as io]
            [utos-msgpack.gui :as gui])
  (:import [java.awt Desktop Desktop$Action]
           java.net.URI
           utos_msgpack.views.controllers.About)
  (:gen-class))

(def ^:const form "about")
(def ^:const title "UTOS MsgPack - About")

(defn open-vikingmakt []
  (if (Desktop/isDesktopSupported)
    (let [desktop (Desktop/getDesktop)]
      (if (.isSupported desktop Desktop$Action/BROWSE)
        (.browse desktop (new URI "https://vikingmakt.com.br"))))))

(defn initialize [{:keys [^About contr stage] :as state}]
  (gui/set-title stage title)

  (-> contr .lbCopyright (gui/set-text "Copyright (c) 2016 Viking Makt"))
  (-> contr .taDescription (gui/set-text (slurp (io/resource "ABOUT"))))

  (-> contr .lbCopyright (gui/on-click open-vikingmakt)))

(defn start []
  (let [stage (gui/create-stage)
        contr (new About)
        state {:contr contr
               :stage stage}]
    (gui/set-stage-min-dimension stage {:width 640 :height 360})
    (gui/setup-scene stage form contr)
    (initialize state)
    (gui/show! stage)))
