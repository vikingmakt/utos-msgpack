(ns utos-msgpack.views.main
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [utos-msgpack.concurrent :refer [set-on-succeeded]]
            [utos-msgpack.gui :as gui]
            [utos-msgpack.i18n :as i18n]
            [utos-msgpack.view-models.main :as model]
            [utos-msgpack.views.about :as about-view])
  (:import javafx.collections.FXCollections
           javafx.util.StringConverter
           utos_msgpack.views.controllers.Main)
  (:gen-class))

(def ^:const form "main")
(def ^:const title "UTOS MsgPack - Console")

(defprotocol Delimiter
  (delimiter-prompt [this])
  (delimiter-string [this]))

(defn get-conn-type [{:keys [^Main contr]}]
  (if (-> contr .rbTcp gui/radio-selected?)
    :tcp
    :udp))

(defn conn-type-changed [{:keys [^Main contr] :as state}]
  (case (get-conn-type state)
    :udp (-> contr .gpUdp (gui/set-visible true))
    :tcp (-> contr .gpUdp (gui/set-visible false))))

(defn disable-elements-connecting [^Main contr v]
  (-> contr .tfHost (gui/set-disable v))
  (-> contr .tfPort (gui/set-disable v))
  (-> contr .rbTcp (gui/set-disable v))
  (-> contr .rbUdp (gui/set-disable v))
  (-> contr .btnConnDisconn (gui/set-disable v)))

(defn open-connection [{:keys [^Main contr vm] :as state}]
  (let [host (-> contr .tfHost gui/get-text string/trim not-empty)
        port (-> contr .tfPort gui/get-text string/trim not-empty)
        local-port (-> contr .tfLocalPort gui/get-text not-empty)]
    (cond
      (nil? host) (gui/show-alert :error (i18n/get-string "wrong_param") (str (i18n/get-string "ip_hostname") \space (i18n/get-string "cannot_be_empty")) nil true)
      (nil? port) (gui/show-alert :error (i18n/get-string "wrong_param") (str (i18n/get-string "word.port") \space (i18n/get-string "cannot_be_empty")) nil true)
      :else
      (do
        (disable-elements-connecting contr true)
        (doto (model/connect vm (get-conn-type state) host port {:local-port local-port})
          (set-on-succeeded
           (fn [{:keys [ok? error-header error-msg]}]
             (when-not ok?
               (disable-elements-connecting contr false)
               (gui/show-alert :error error-header error-msg nil true)))))))))

(defn close-connection [{:keys [vm]}]
  (try
    (model/close vm)
    (catch Exception e)))

(defn request-close [{:keys [vm] :as state}]
  (close-connection state)
  (System/exit 0))

(defn open-about []
  (about-view/start))

(defn send-msg [{:keys [^Main contr vm]}]
  (let [msg (-> contr .taInput gui/get-text)
        delimiter (-> contr .cbInputDelimiter .getValue)
        delimiter (if delimiter (-> delimiter delimiter-string not-empty))
        timeout (-> contr .tfTimeout gui/get-text not-empty)]
    (-> contr .taOutput (gui/set-disable true))
    (-> contr .btnSend (gui/set-disable true))
    (-> contr .btnLoadFile (gui/set-disable true))
    (doto (model/send-msg vm msg delimiter timeout)
      (set-on-succeeded
       (fn [_]
         (-> contr .taOutput (gui/set-disable false))
         (-> contr .btnSend (gui/set-disable false))
         (-> contr .btnLoadFile (gui/set-disable false)))))))

(defn load-json-file [{:keys [^Main contr stage vm]}]
  (if-let [f (gui/file-chooser
              stage
              :title (i18n/get-string "choose_file_send")
              :ext [[(i18n/get-string "json_file") ["*.json"]]]
              :op :open)]

    (with-open [is (io/input-stream f)]
      (if-let [msg (try
                     (json/parse-stream (io/reader is))
                     (catch Exception e))]
        (-> contr .taInput (gui/set-text (json/generate-string msg {:escape-non-ascii false
                                                                    :pretty (-> json/default-pretty-print-options
                                                                                (assoc :indent-arrays? true)
                                                                                json/create-pretty-printer)})))
        (gui/show-alert :error (i18n/get-string "decode_error") (i18n/get-string "json_is_invalid") nil true)))))

(defn conn-disconn [{:keys [vm] :as state}]
  (case (model/get-status vm)
    :OPEN (close-connection state)
    :CONN-FAILED (open-connection state)
    :CLOSED (open-connection state)
    :REMOTE-CLOSED (open-connection state)
    :NEW nil))

(defn set-status-text [{:keys [^Main contr]} status]
  (case status
    :REMOTE-CLOSED
    (do
      (-> contr .lbStatus (gui/set-text (i18n/get-string "server_closed_conn")))
      (-> contr .btnConnDisconn (gui/set-text (i18n/get-string "word.connect"))))
    :CLOSED
    (do
      (-> contr .lbStatus (gui/set-text (i18n/get-string "word.disconnected")))
      (-> contr .btnConnDisconn (gui/set-text (i18n/get-string "word.connect"))))
    :OPEN
    (do
      (-> contr .lbStatus (gui/set-text (i18n/get-string "word.connected")))
      (-> contr .btnConnDisconn (gui/set-text (i18n/get-string "word.disconnect"))))
    :NEW
    (do
      (-> contr .lbStatus (gui/set-text (i18n/get-string "word.connecting")))
      (-> contr .btnConnDisconn (gui/set-text (i18n/get-string "word.connect"))))
    :CONN-FAILED
    (do
      (-> contr .lbStatus (gui/set-text (i18n/get-string "connection_failed")))
      (-> contr .btnConnDisconn (gui/set-text (i18n/get-string "word.connect"))))))

(defn status-changed [{:keys [^Main contr] :as state} status]
  (case status
    :REMOTE-CLOSED
    (do
      (-> contr .taOutput (gui/set-disable true))
      (-> contr .btnSend (gui/set-disable true))
      (-> contr .tfHost (gui/set-disable false))
      (-> contr .tfPort (gui/set-disable false))
      (-> contr .rbTcp (gui/set-disable false))
      (-> contr .rbUdp (gui/set-disable false)))
    :CLOSED
    (do
      (-> contr .taOutput (gui/set-disable true))
      (-> contr .btnSend (gui/set-disable true))
      (-> contr .tfHost (gui/set-disable false))
      (-> contr .tfPort (gui/set-disable false))
      (-> contr .rbTcp (gui/set-disable false))
      (-> contr .rbUdp (gui/set-disable false)))
    :OPEN
    (do
      (-> contr .btnConnDisconn (gui/set-disable false))
      (-> contr .tfHost (gui/set-disable true))
      (-> contr .tfPort (gui/set-disable true))
      (-> contr .rbTcp (gui/set-disable true))
      (-> contr .rbUdp (gui/set-disable true))
      (-> contr .taOutput (gui/set-disable false))
      (-> contr .btnSend (gui/set-disable false)))
    :NEW
    (do
      (-> contr .tfHost (gui/set-disable true))
      (-> contr .tfPort (gui/set-disable true))
      (-> contr .rbTcp (gui/set-disable true))
      (-> contr .rbUdp (gui/set-disable true))
      (-> contr .btnConnDisconn (gui/set-disable true))
      (-> contr .taOutput (gui/set-text "")))
    :CONN-FAILED
    (do
      (-> contr .tfHost (gui/set-disable false))
      (-> contr .tfPort (gui/set-disable false))
      (-> contr .rbTcp (gui/set-disable false))
      (-> contr .rbUdp (gui/set-disable false))
      (-> contr .btnConnDisconn (gui/set-disable false))))
  (set-status-text state status))

(defn model-changed-handler [{:keys [^Main contr] :as state} prop-name _ old-val new-val]
  (if-not (= old-val new-val)
    (case prop-name
      :conn-status
      (status-changed state new-val)
      :output
      (-> contr .taOutput (gui/set-text new-val)))))

(defn change-lang [lang country]
  (i18n/set-bundle lang country))

(defn lang-changed [{:keys [^Main contr vm] :as state} key lang country]
  (-> contr .meLang (gui/set-text (i18n/get-string "word.translation")))
  (-> contr .meExit (gui/set-text (i18n/get-string "word.exit")))
  (-> contr .miAbout (gui/set-text (i18n/get-string "word.about")))
  (-> contr .miExit (gui/set-text (i18n/get-string "sure_to_exit")))
  (-> contr .lbHost (gui/set-text (i18n/get-string "ip_hostname")))
  (-> contr .lbPort (gui/set-text (i18n/get-string "word.port")))
  (-> contr .lbLocalPort (gui/set-text (i18n/get-string "local_port")))
  (-> contr .lbInput (gui/set-text (i18n/get-string "word.input")))
  (-> contr .lbDelimiter (gui/set-text (i18n/get-string "word.delimiter")))
  (-> contr .lbTimeout (gui/set-text (i18n/get-string "word.timeout")))
  (-> contr .btnSend (gui/set-text (i18n/get-string "word.send")))
  (-> contr .lbOutput (gui/set-text (i18n/get-string "word.output")))
  (-> contr .btnLoadFile (gui/set-text (i18n/get-string "main.file_load")))
  (set-status-text state (model/get-status vm)))

(defn initialize [{:keys [^Main contr stage vm] :as state}]
  (gui/set-title stage title)
  (gui/set-resizable stage true)

  (-> contr .tfTimeout (gui/set-text "30000"))
  (-> contr .taOutput (gui/set-disable true))
  (-> contr .btnSend (gui/set-disable true))
  (-> contr .gpUdp (gui/set-visible false))

  (gui/add-shortcut stage :ctrl "Q" #(request-close state))
  (gui/on-close-request stage #(request-close state))
  (-> contr .miExit (gui/on-menu-click #(request-close state)))
  (-> contr .miEnUs (gui/on-menu-click #(change-lang "en" "US")))
  (-> contr .miPtBr (gui/on-menu-click #(change-lang "pt" "BR")))
  (-> contr .miAbout (gui/on-menu-click open-about))
  (-> contr .btnConnDisconn (gui/on-button-click #(conn-disconn state)))
  (-> contr .btnSend (gui/on-button-click #(send-msg state)))
  (-> contr .btnLoadFile (gui/on-button-click #(load-json-file state)))
  (-> contr .rbUdp (gui/on-toggle-changed (fn [& args] (conn-type-changed state))))

  (let [oal (FXCollections/observableArrayList)]
    (doseq [[n c] {"" "" "\\n" "\n" "\\r\\n" "\r\n"}]
      (.add oal (reify
                  Delimiter
                  (delimiter-prompt [_] n)
                  (delimiter-string [_] c))))
    (-> contr .cbInputDelimiter (.setConverter (proxy [StringConverter] []
                                                 (toString [o]
                                                   (delimiter-prompt o)))))
    (-> contr .cbInputDelimiter (.setItems oal))))

(defn start [stage]
  (let [contr (new Main)
        vm (model/create-view)
        state {:contr contr
               :stage stage
               :vm vm}]
    (i18n/add-listener :main #(lang-changed state %1 %2 %3))
    (gui/set-stage-min-dimension stage {:width 640 :height 360})
    (model/add-handler vm #(model-changed-handler state %1 %2 %3 %4))
    (gui/setup-scene stage form contr)
    (initialize state)
    (gui/show! stage)))
