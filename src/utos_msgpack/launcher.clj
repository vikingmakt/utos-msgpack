(ns utos-msgpack.launcher
  (:require [clojure.java.io :as io]
            [clojure.tools.logging :as log]
            [utos-msgpack.gui :as gui]
            [utos-msgpack.views.main :as main])
  (:import javafx.application.Application)
  (:gen-class :extends javafx.application.Application))

(defn -start [app stage]
  (try
    (do
      (gui/add-icon stage (io/resource "gui/fx/asserts/images/utos.png"))
      (main/start stage))
    (catch Throwable e
      (throw e))))

(defn -main [& args]
  (defonce force-toolkit-init (new javafx.embed.swing.JFXPanel))
  (Application/launch utos_msgpack.launcher (into-array String args)))
