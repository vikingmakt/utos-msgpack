package utos_msgpack.views.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class Main {
    @FXML public Button btnConnDisconn;
    @FXML public Button btnSend;
    @FXML public Button btnLoadFile;
    @FXML public ComboBox cbInputDelimiter;
    @FXML public GridPane gpUdp;
    @FXML public Label lbDelimiter;
    @FXML public Label lbHost;
    @FXML public Label lbInput;
    @FXML public Label lbLocalPort;
    @FXML public Label lbOutput;
    @FXML public Label lbPort;
    @FXML public Label lbStatus;
    @FXML public Label lbTimeout;
    @FXML public Menu meExit;
    @FXML public Menu meLang;
    @FXML public MenuItem miAbout;
    @FXML public MenuItem miEnUs;
    @FXML public MenuItem miExit;
    @FXML public MenuItem miPtBr;
    @FXML public RadioButton rbTcp;
    @FXML public RadioButton rbUdp;
    @FXML public TextArea taInput;
    @FXML public TextArea taOutput;
    @FXML public TextField tfHost;
    @FXML public TextField tfLocalPort;
    @FXML public TextField tfPort;
    @FXML public TextField tfTimeout;
}
