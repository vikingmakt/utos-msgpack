package utos_msgpack.views.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class About {
    @FXML public Label lbCopyright;
    @FXML public TextArea taDescription;
}
